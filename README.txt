Internationalisation Helper (i18n_helper)

DESCRIPTION:

i18n Helper provides additional useful functions to i18n

1. Fields synchronised by i18n_sync can be hidden or set to read-only in node views and node edit forms.
2. Fields not synchronised by i18n may be blanked when new translations are created.
3. A "Create translation stubs" action is provided, which when executed by a VBO (Views Bulk Operations) or Rule allows bulk or automatic creation of new translations.

USAGE:

Enable and configure at admin/content/types/my_type/edit under Multilanguage options